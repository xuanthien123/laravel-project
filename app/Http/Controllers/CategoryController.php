<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category;
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = Category::root()->get();
        return view('pages.categories', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $this->category->create($request->all());
        return redirect(route('categories'));
    }
    public function create()
    {
        $categories = Category::root()->get();
        return view('pages.category.create', compact('categories'));
    }
    public function delete($id)
    {
        $category = $this->category->findOrFail($id);
        $category->delete();
        return redirect(route('categories'));
    }
    public function edit($id)
    {
        $categories = Category::root()->get();
        $categoryEdit = $this->category->findOrFail($id);
        return view('pages.category.edit', compact('categoryEdit', 'categories'));
    }
    public function update(CategoryRequest $request, $id)
    {
        $category = $this->category->findOrFail($id);
        $category->update($request->all());
        return redirect(route('categories'));
    }

}
