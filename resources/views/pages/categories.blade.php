@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary row justify-content-sm-between">
                            <div class="column justify-center">
                                <h2 class="card-title ">Danh sách danh mục</h2>
                            </div>
                            <div class="column right-0">
                                <a class="btn btn-success" href="{{route('categories.create')}}">Thêm danh mục</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">

                                    <table class="table">
                                        <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Tên danh mục
                                        </th>
                                        <th>
                                            Danh mục cha
                                        </th>
                                        <th colspan="2">
                                            Công cụ
                                        </th>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $category)
                                            <form action="{{route('categories.delete', $category->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <tr style="background: darkgray">
                                                    <th>{{$category->id}}</th>
                                                    <th>{{$category->name}}</th>
                                                    <th>{{$category->parent_id}}</th>
                                                    <td style="width: 5%">
                                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xóa danh mục này?')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-primary" href="{{route('categories.edit', $category->id)}}"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            </form>
                                                @foreach($category->children as $child)<i class="fas fa-edit"></i>
                                                <form action="{{route('categories.delete', $child->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <tr>
                                                        <th>{{$child->id}}</th>
                                                        <th>{{$child->name}}</th>
                                                        <th>{{$category->name}}</th>
                                                        <td style="width: 5%">
                                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xóa danh mục này?')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-primary" href="{{route('categories.edit', $child->id)}}"><i class="fa fa-edit"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </form>
                                        @endforeach
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

