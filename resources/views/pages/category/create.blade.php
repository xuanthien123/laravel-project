@extends('layouts.app', ['activePage' => 'table', 'titlePage' => __('Table List')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="column justify-center">
                                <h3 class="card-title ">Tạo mới danh mục</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex justify-content-xl-center">
                                <form style="width: 50%" method="post" action="{{route('categories.store')}}">
                                    @csrf
                                    <br>
                                    <div class="form-group">
                                        <label for="CategoryName">Tên danh mục</label>
                                        <input type="text" name="name" class="form-control" id="CategoryName" aria-describedby="emailHelp" placeholder="Nhập tên" autofocus>
                                        @error('name')
                                        <span id="name-error" class="error text-danger" style="display: block"> {{$message}}</span>
                                        @enderror
                                    </div>
                                    <div>
                                        <label for="ParentCategory">Danh mục cha</label>
                                        <div>
                                            <select id="ParentCategory" name="parent_id">
                                                <option selected value="" >Chọn danh mục cha</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Tạo mới</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
