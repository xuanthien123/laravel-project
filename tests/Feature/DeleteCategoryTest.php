<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_category()
    {
        $this->actingAs(User::factory()->create());
        $category = Category::factory()->create();
        $countBeforeDelete = Category::count();
        $response = $this->delete(route('categories.delete', $category->id));

        $response->assertRedirect(route('categories'));
        $countAfterDelete = Category::count();
        $this->assertEquals($countAfterDelete+1, $countBeforeDelete);
    }

    /** @test */
    public function unauthenticated_user_can_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->delete(route('categories.delete', $category->id));

        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_user_can_not_delete_category_if_category_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $categoryID = -1;
        $response = $this->delete(route('categories.delete', $categoryID));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
