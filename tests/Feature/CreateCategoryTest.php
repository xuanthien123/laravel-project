<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_view_create_category_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get(route('categories.create'));

        $response->assertViewIs('pages.category.create');
    }
    /** @test */
    public function authenticated_user_can_create_new_category()
    {
        $this->actingAs(User::factory()->create());
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertRedirect(route('categories'));
        $this->assertDatabaseHas('categories', [
            'name' => $category['name'],
        ]);
    }
    /** @test */
    public function unauthenticated_user_can_create_new_category()
    {
        $category = Category::factory()->make()->toArray();
        $response = $this->post(route('categories.store'), $category);

        $response->assertRedirect('login');
    }
}
