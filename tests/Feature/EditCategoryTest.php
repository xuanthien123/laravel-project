<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_category()
    {
        $this->actingAs(User::factory()->create());
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name()
        ];
        $response = $this->put(route('categories.update', $category->id), $dataUpdate);

        $response->assertRedirect(route('categories'));
        $this->assertDatabaseHas('categories', [
            'name' => $dataUpdate['name'],
            'id' => $category->id
        ]);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name()
        ];
        $response = $this->put(route('categories.update', $category->id), $dataUpdate);

        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_category_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $categoryID = -1;
        $dataUpdate = [
            'name' => $this->faker->name()
        ];
        $response = $this->put(route('categories.update', $categoryID), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $category = Category::factory()->create();
        $dataUpdate = [
            'name' => ''
        ];
        $response = $this->put(route('categories.update', $category->id), $dataUpdate);

        $response->assertSessionHasErrors('name');
    }
}
