<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListCategoriesTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_categories()
    {
        $this->actingAs(User::factory()->create());
        $category = Category::factory(['parent_id' => 1])->create();
        $response = $this->get(route('categories'));

        $response->assertViewIs('pages.categories');
        $response->assertSee([$category->id, $category->name]);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $category = Category::factory(['parent_id' => 1])->create();
        $response = $this->get(route('categories'));

        $response->assertRedirect('login');
    }
}
